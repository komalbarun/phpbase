<?php declare(strict_types=1);

require_once dirname(__DIR__).DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

$whoops = new \Whoops\Run;
if( (int)$_ENV['DEBUG'] === 1){
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
}else{
    $whoops->pushHandler(new \Whoops\Handler\PlainTextHandler);
}
$whoops->register();

/**
 * BEGIN ROUTING
 */
$klein = new \Klein\Klein();

/**
 * Begin Tutorial Routes
 */
$Tuto = new \Phpbase\TutorialModule\Controllers\TutoController();
use Phpbase\TutorialModule\Routing\Routes;
// http(s)://domainname/tuto/
Routes::loadRoutes($klein, $Tuto);
/**
 * End Tutorial Routes
 */


$klein->dispatch();
/**
 * END ROUTING
 */