<?php declare(strict_types=1);

namespace Phpbase\TutorialModule\Routing;
use klein\Klein;
use Phpbase\TutorialModule\Controllers\TutoController;


class Routes
{
    public static function loadRoutes(Klein $klein, TutoController $controller){
        $klein->with('/tuto', function() use ($klein, $controller){
           $klein->respond('GET', '/', function($req, $res)
                use ($controller)
           {
                echo $controller->index();
           });
        });
    }
}