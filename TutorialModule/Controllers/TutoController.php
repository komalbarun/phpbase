<?php declare(strict_types=1);


namespace Phpbase\TutorialModule\Controllers;

use Phpbase\Core\Controllers\CoreController;

class TutoController extends CoreController
{
    protected $twig;
    
    public function __construct()
    {
        parent::__construct();

        /**
         * To call addPath OR prependPath in all controllers.
         */
        $this->loader->addPath( $this->getModuleViews() );
    }

    public function index(){
        echo $this->twig->render('index.twig.html');
    }

    /**
     * Need to define this in all controllers
     */
    public function getModuleViews()
    {
        return dirname(__DIR__).DIRECTORY_SEPARATOR.'views';
    }
}