<?php declare(strict_types=1);

if(php_sapi_name()!=="cli"){
    throw new Exception("Can only be run on command line", 1);
}

$options = getopt('', ['modulename:']);
$modulename = $options['modulename'] ?? throw new \Exception('wrong option!');

function publishAssets($moduleName){
    if( strstr(__DIR__, 'vendor') ){
        $baseDir = explode('vendor', __DIR__)[0];
        $basename = basename($baseDir);
    }else{
        $baseDir = explode('bin', __DIR__)[0];
        $basename = basename($baseDir);
    }

    if($basename=='vendor'){
        $appRoot = dirname( $baseDir ); 
    }else{
        $appRoot = $baseDir;
    }

    $dirsep = DIRECTORY_SEPARATOR;
    $moduleName = ucfirst($moduleName);
    $publicAssetsRoot = $appRoot.$dirsep.'public';
    $moduleAssetsFolder = $appRoot.$moduleName.'Module'.$dirsep.'assets';
    $innerModuleAssets = scandir($moduleAssetsFolder);
    $ignores = ['.', '..'];

    foreach($innerModuleAssets as $folder){
        if( in_array($folder, $ignores) ){ continue; }
        $files = glob($moduleAssetsFolder.$dirsep.$folder.$dirsep."*.{{$folder},png,PNG,jpeg,JPEG,JPG,jpg,svg,SVG}", GLOB_BRACE);
        foreach($files as $file ){
            $filename = basename($file);
            $destinationFolder = $publicAssetsRoot.$dirsep.'assets'.$dirsep.$moduleName.'Module'.$dirsep.$folder;
            $destinationFileName = $destinationFolder.$dirsep.$dirsep.$filename;
            $fileExists = is_file($destinationFileName);
            if( !$fileExists){
                if( !is_dir( $destinationFolder ) ){
                    mkdir( $destinationFolder, 0777, true);
                }

                if( is_dir($destinationFolder) ){
                    copy( $file, $destinationFileName);
                }
            }
        }
    }
}

publishAssets($modulename);