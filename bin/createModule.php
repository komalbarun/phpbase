<?php declare(strict_types=1);

if(php_sapi_name()!=="cli"){
    throw new Exception("Can only be run on command line", 1);
}

$options = getopt('n:', $long_options=["modulename:"]);

$name = $options['n'] ?? $options['modulename'] ?? throw new \Exception('wrong option!');

function checkAndCreatePaths(array $paths){
    foreach($paths as $path){
        if( !is_dir($path ) ){
            mkdir( $path );
        }
    }
}

function writeToAndCloseFile($fileHandle, string $text){
    fwrite($fileHandle, $text);
    fclose($fileHandle);
}

function createIndexFile(string $folderPath, string $fileName){
    $fileName = $folderPath.DIRECTORY_SEPARATOR.$fileName;
    $fh= fopen($fileName, 'w');

    $contents = <<<'IndexFile'
    <?php declare(strict_types=1);

    require_once dirname(__DIR__).DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';
    
    $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
    $dotenv->load();

    $whoops = new \Whoops\Run;
    if( (int)$_ENV['DEBUG'] === 1){
        $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
    }else{
        $whoops->pushHandler(new \Whoops\Handler\PlainTextHandler);
    }
    $whoops->register();

    /**
     * BEGIN ROUTING
     */
    $klein = new \Klein\Klein();

    /**
     * Begin Tutorial Routes
     */
    $Tuto = new \Phpbase\TutorialModule\Controllers\TutoController();
    use Phpbase\TutorialModule\Routing\Routes;
    // http(s)://domainname/tuto/
    Routes::loadRoutes($klein, $Tuto);
    /**
     * End Tutorial Routes
     */


    $klein->dispatch();
    /**
     * END ROUTING
     */
    IndexFile;

    writeToAndCloseFile($fh, $contents);
}

function createPSR4Namespace(string $rootFolder, string $modulename){
    $composerFilePath = $rootFolder.'composer.json';
    if( is_file($composerFilePath) ){
        $composerJson = json_decode(file_get_contents( $composerFilePath), true);
        $composerJson['autoload']['psr-4'][$modulename.'Module\\'] = $rootFolder.$modulename.'Module';
        file_put_contents($composerFilePath, json_encode($composerJson, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    }
}

function createModule($moduleName){
    if( strstr(__DIR__, 'vendor') ){
        $baseDir = explode('vendor', __DIR__)[0];
        $basename = basename($baseDir);
    }else{
        $baseDir = explode('bin', __DIR__)[0];
        $basename = basename($baseDir);
    }

    if($basename=='vendor'){
        $appRoot = dirname( $baseDir ); 
    }else{
        $appRoot = $baseDir;
    }

    $dirsep = DIRECTORY_SEPARATOR;
    $moduleName = ucfirst($moduleName);
    $moduleFolderPath = $appRoot.$dirsep.$moduleName.'Module';
    
    $folders = [
        'root' => $moduleFolderPath,
        'public' => $appRoot.'public',
        'publicAssets' => $appRoot.'public'.$dirsep.'assets',
        'publicAssetsJs' => $appRoot.'public'.$dirsep.'assets'.$dirsep.'js',
        'publicAssetsCss' => $appRoot.'public'.$dirsep.'assets'.$dirsep.'css',
        'publicAssetsImg' => $appRoot.'public'.$dirsep.'assets'.$dirsep.'img',
        'controllers' => $moduleFolderPath.$dirsep.'Controllers',
        'models' => $moduleFolderPath.$dirsep.'Models',
        'routing' => $moduleFolderPath.$dirsep.'Routing',
        'views' => $moduleFolderPath.$dirsep.'Views',
        'assets' => $moduleFolderPath.$dirsep.'assets',
        'js' => $moduleFolderPath.$dirsep.'assets'.$dirsep.'js',
        'css' => $moduleFolderPath.$dirsep.'assets'.$dirsep.'css',
        'img' => $moduleFolderPath.$dirsep.'assets'.$dirsep.'img',
    ];

    checkAndCreatePaths($folders);

    $moduleCheckerFilePath = $moduleFolderPath.DIRECTORY_SEPARATOR.'module.txt';
    if( !is_file( $moduleCheckerFilePath) ){
        touch($moduleCheckerFilePath);
    }

    createIndexFile($folders['public'], 'index.php');
    createPSR4Namespace($appRoot, $moduleName);
}


createModule($name);