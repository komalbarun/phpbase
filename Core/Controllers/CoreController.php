<?php declare(strict_types=1);

namespace Phpbase\Core\Controllers;
use Phpbase\Core\Controllers\ControllerInterface;

class CoreController implements ControllerInterface
{
    protected $loader;
    protected $twig;

    public function __construct()
    {   
        $this->loader = new \Twig\Loader\FilesystemLoader($_ENV['GLOBAL_VIEWS_PATH']);
        
        $this->twig = new \Twig\Environment($this->loader, [
            'cache' => $_ENV['GLOBAL_VIEWS_PATH'].DIRECTORY_SEPARATOR.'cache',
            'debug' => (int) $_ENV['DEBUG'] === 1 ? true : false
        ]);

        $this->loader->addPath( $this->getModuleViews() );
    }

    public function getModuleViews()
    {
        return dirname(__DIR__).DIRECTORY_SEPARATOR.'views';
    }
}