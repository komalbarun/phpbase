<?php declare(strict_types=1);

 namespace Phpbase\Core\Controllers;

interface ControllerInterface{
    public function getModuleViews();
}