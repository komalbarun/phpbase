### Base 'No Frameworks' HMVC blank 'project'

`composer create-project komalbarun/hmvcbase projectName` to start new project.

`composer require komalbarun/hmvcbase` to use as a vendor package.

`php bin/vendor/createModule.php --modulename=Name` OR `php vendor/bin/createModule.php --name=Name` (Name without 'Module' part ) to create module scaffolding.

`php bin/collectStatic.php --modulename=Tutorial` OR `php vendor/bin/collectStatic.php --modulename=modulename`( modulename without 'Module' ) to copy module's assets files to public/asset/modulename. THIS DOES NOT MINIFY FILES, ONLY COPIES THEM. ( http3 ftw :D )

I believe I have all the basics set up to start developping with this 'library'. 
If you don't know what you are doing, use Yii3 or latest Laravel to start your
project!